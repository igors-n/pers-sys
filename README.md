### Dependecies
* Lando 3.0.0 R.C.1. later version

#### Running locally
* Run commands
* `lando start`
* `lando composer install -d app`
* `lando php artisan migrate --seed`
* Open url `http://pers-system.lndo.site`
* Login with default user `admin@admin.com`, password `321`
