@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Workhours</div>
                    <div class="card-body">
                        @if (!\Auth::user()->hasRole(['Employee']))
                            <a href="{{ url('/admin/work-hours/create') }}" class="btn btn-success btn-sm"
                               title="Add New WorkHour">
                                <i class="fa fa-plus" aria-hidden="true"></i> Add New
                            </a>
                        @endif

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/work-hours', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..."
                                   value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/><br/>
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/work-hours', 'class' => 'form-inline my-2 my-lg-0 float-left', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control date-picker" name="date_from"
                                   placeholder="Date from..."
                                   value="{{ request('date_from') }}">
                        </div>

                        <small style="padding: 10px;">to</small>

                        <div class="input-group">
                            <input type="text" class="form-control date-picker" name="date_to" placeholder="Date to..."
                                   value="{{ request('date_to') }}">
                        </div>

                        <div class="input-group" style="margin-left: 20px;">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User Id</th>
                                    <th>Date</th>
                                    <th>Hours</th>
                                    @if (!\Auth::user()->hasRole(['Employee']))
                                        <th>Actions</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($workhours as $item)
                                    <tr>
                                        <td>{{ $loop->iteration or $item->id }}</td>
                                        <td>{{ $item->user->name }} {{ $item->user->last_name }}</td>
                                        <td>{{ $item->date }}</td>
                                        <td>{{ $item->hours }}</td>
                                        @if (!\Auth::user()->hasRole(['Employee']))
                                            <td>
                                                <a href="{{ url('/admin/work-hours/' . $item->id) }}"
                                                   title="View WorkHour">
                                                    <button class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i>
                                                    </button>
                                                </a>
                                                <a href="{{ url('/admin/work-hours/' . $item->id . '/edit') }}"
                                                   title="Edit WorkHour">
                                                    <button class="btn btn-primary btn-sm"><i
                                                            class="fa fa-pencil-square-o"
                                                            aria-hidden="true"></i>
                                                    </button>
                                                </a>
                                                {!! Form::open([
                                                    'method' => 'DELETE',
                                                    'url' => ['/admin/work-hours', $item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete WorkHour',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div
                                class="pagination-wrapper"> {!! $workhours->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
