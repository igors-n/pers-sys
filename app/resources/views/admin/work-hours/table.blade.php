@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Workhours Table</div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/work-hours/table', 'class' => 'form-inline my-2 my-lg-0 float-left', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control date-picker" name="date_from"
                                   placeholder="Date from..."
                                   value="{{ request('date_from') }}">
                        </div>

                        <small style="padding: 10px;">to</small>

                        <div class="input-group">
                            <input type="text" class="form-control date-picker" name="date_to" placeholder="Date to..."
                                   value="{{ request('date_to') }}">
                        </div>

                        <div class="input-group" style="margin-left: 20px;">
                            <button class="btn btn-secondary" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        {!! Form::close() !!}
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>User</th>
                                    <th>Hours Worked</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($workhours as $workhour)
                                    <tr>
                                        <td>{{ $loop->iteration }}.
                                        <td>{{ $workhour->user->name . ' ' . $workhour->user->last_name }}</td>
                                        <td>{{ $workhour->sum }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
