<div class="form-group{{ $errors->has('user_id') ? ' has-error' : ''}}">
    {!! Form::label('user_id', 'User: ', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $users, isset($workhour->user) ? $workhour->user->id : [], ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date') ? 'has-error' : ''}}">
    {!! Form::label('date', 'Date', ['class' => 'control-label']) !!}
    {!! Form::text('date', isset($workhour->date) ? $workhour->date : date('Y-m-d'), ('required' == 'required') ? ['class' => 'form-control date-picker', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('hours') ? 'has-error' : ''}}">
    {!! Form::label('hours', 'Hours', ['class' => 'control-label']) !!}
    {!! Form::number('hours', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('hours', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
