<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', 'Last name: ', ['class' => 'control-label']) !!}
    {!! Form::text('last_name', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', 'Phone: ', ['class' => 'control-label']) !!}
    {!! Form::text('phone', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('personal_code') ? ' has-error' : ''}}">
    {!! Form::label('personal_code', 'Personal code: ', ['class' => 'control-label']) !!}
    {!! Form::text('personal_code', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('personal_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
    {!! Form::label('address', 'Address: ', ['class' => 'control-label']) !!}
    {!! Form::text('address', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'control-label']) !!}
    {!! Form::email('email', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password', 'Password: ', ['class' => 'control-label']) !!}
    @php
        $passwordOptions = ['class' => 'form-control'];
        if ($formMode === 'create') {
            $passwordOptions = array_merge($passwordOptions, ['required' => 'required']);
        }
    @endphp
    {!! Form::password('password', $passwordOptions) !!}
    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}

    <br/>
    <div class="alert alert-warning" role="alert">
        Password should contain:
        <ul>
            <li>8 or more symbols</li>
            <li>At least one number</li>
            <li>At least one lower case letter</li>
            <li>At least one upper case letter</li>
        </ul>
    </div>
</div>
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'Role: ', ['class' => 'control-label']) !!}
    {!! Form::select('roles[]', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control', 'multiple' => TRUE]) !!}
</div>
<div class="form-group{{ $errors->has('company_id') ? ' has-error' : ''}}">
    {!! Form::label('company_id', 'Company: ', ['class' => 'control-label']) !!}
    {!! Form::select('company_id', $companies, isset($user->company) ? $user->company->id : [], ['class' => 'form-control']) !!}
</div>
<div class="form-group{{ $errors->has('unit_id') ? ' has-error' : ''}}">
    {!! Form::label('unit_id', 'Unit: ', ['class' => 'control-label']) !!}
    {!! Form::select('unit_id', $units, $user->unit_id ?? NULL, ['class' => 'form-control']) !!}
</div>
<div class="form-group{{ $errors->has('position') ? ' has-error' : ''}}">
    {!! Form::label('position', 'Position: ', ['class' => 'control-label']) !!}
    {!! Form::text('position', NULL, ['class' => 'form-control']) !!}
    {!! $errors->first('position', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
