@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">News</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @foreach($news as $item)
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $item->name }}</h5>
                                        <p class="card-text">{{ strlen($item->text) > 200 ? mb_substr($item->text, 0, 200) . '...' : $item->text }}</p>
                                        <a href="/admin/news/view/{{ $item->id }}" class="btn btn-primary">Read more</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
