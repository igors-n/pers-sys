@extends('layouts.backend')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">News</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $news->name }}</h5>
                                    <p class="card-text">{{ $news->text }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
