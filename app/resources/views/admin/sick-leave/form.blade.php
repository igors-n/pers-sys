<div class="form-group{{ $errors->has('date_from') ? 'has-error' : ''}}">
    {!! Form::label('date_from', 'Date From', ['class' => 'control-label']) !!}
    {!! Form::date('date_from', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_from', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_to') ? 'has-error' : ''}}">
    {!! Form::label('date_to', 'Date To', ['class' => 'control-label']) !!}
    {!! Form::date('date_to', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_to', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('user_id') ? ' has-error' : ''}}">
    {!! Form::label('user_id', 'User: ', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $users, isset($sickleave->user) ? $sickleave->user->id : [], ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
