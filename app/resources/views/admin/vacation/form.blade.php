<div class="form-group{{ $errors->has('date_from') ? 'has-error' : ''}}">
    {!! Form::label('date_from', 'Date From', ['class' => 'control-label']) !!}
    {!! Form::text('date_from', isset($vacation->date_from) ? $vacation->date_from : date('Y-m-d'), ('required' == 'required') ? ['class' => 'form-control date-picker', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_from', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('date_to') ? 'has-error' : ''}}">
    {!! Form::label('date_to', 'Date To', ['class' => 'control-label']) !!}
    {!! Form::text('date_to', isset($vacation->date_to) ? $vacation->date_to : date('Y-m-d'), ('required' == 'required') ? ['class' => 'form-control date-picker', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_to', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('user_id') ? ' has-error' : ''}}">
    {!! Form::label('user_id', 'User: ', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $users, isset($vacation->user) ? $vacation->user->id : [], ['class' => 'form-control']) !!}
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
    {!! Form::label('type', 'Type: ', ['class' => 'control-label']) !!}
    {!! Form::select('type', \App\Vacation::TYPES, $vacation->type ?? NULL, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
