<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(action('Auth\LoginController@login'));
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


Route::group(['middleware' => 'auth'], function () {
    Route::get('admin', 'Admin\\NewsController@list');

    Route::resource('admin/vacation', 'Admin\\VacationController');
    Route::get('admin/work-hours/table', 'Admin\\WorkHoursController@table');
    Route::resource('admin/work-hours', 'Admin\\WorkHoursController');
    Route::get('admin/news/view/{id}', 'Admin\\NewsController@view');
    Route::resource('admin/users', 'Admin\UsersController');

    Route::group(['middleware' => ['roles'], 'roles' => ['Admin', 'Staff', 'Accountant', 'Manager']], function () {
        Route::resource('admin/company', 'Admin\CompanyController');
        Route::resource('admin/roles', 'Admin\RolesController');
        Route::resource('admin/activitylogs', 'Admin\ActivityLogsController')->only([
            'index', 'show', 'destroy'
        ]);
        Route::resource('admin/education', 'Admin\\EducationController');
        Route::resource('admin/experience', 'Admin\\ExperienceController');
        Route::resource('admin/news', 'Admin\\NewsController');
        Route::resource('admin/company-units', 'Admin\\CompanyUnitsController');
    });
});
