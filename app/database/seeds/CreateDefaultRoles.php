<?php

use Illuminate\Database\Seeder;

class CreateDefaultRoles extends Seeder
{
    private const ROLES = [
        [
            'name' => 'Employee',
            'label' => 'Employee'
        ],
        [
            'name' => 'Accountant',
            'label' => 'Accountant'
        ],
        [
            'name' => 'Manager',
            'label' => 'Manager'
        ],
        [
            'name' => 'Admin',
            'label' => 'Admin'
        ]
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(self::ROLES);
    }
}
