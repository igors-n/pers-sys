<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = DB::table('users')->insertGetId([
            'name' => 'Admin',
            'last_name' => 'Admin',
            'address' => 'Test address',
            'phone' => '1',
            'personal_code' => '1',
            'email' => 'admin@admin.com',
            'password' => bcrypt('321'),
        ]);

        $user = \App\User::find($user);
        $user->assignRole('Admin');
    }
}
