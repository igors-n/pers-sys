<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Vacation;
use Illuminate\Http\Request;

class VacationController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;
        
        if (!empty($keyword)) {
            $vacation = Vacation::where('date_from', 'LIKE', "%$keyword%")
                ->orWhere('date_to', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $vacation = Vacation::latest()->paginate($perPage);
        }
        
        return view('admin.vacation.index', compact('vacation'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $usersObject = User::select('id', 'name', 'last_name')->get();
        $users = [null => '---'];
        foreach ($usersObject as $user) {
            $users[$user->id] = $user->name . ' ' . $user->last_name;
        }
        return view('admin.vacation.create', compact('users'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'date_from' => 'required|date',
            'date_to' => 'required|date|after_or_equal:date_from',
            'user_id' => 'required'
        ]);
        $requestData = $request->all();
        
        Vacation::create($requestData);
        
        return redirect('admin/vacation')->with('flash_message', 'Vacation added!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $vacation = Vacation::findOrFail($id);
        
        return view('admin.vacation.show', compact('vacation'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $vacation = Vacation::findOrFail($id);
        $usersObject = User::select('id', 'name', 'last_name')->get();
        $users = [null => '---'];
        foreach ($usersObject as $user) {
            $users[$user->id] = $user->name . ' ' . $user->last_name;
        }
        
        return view('admin.vacation.edit', compact('vacation', 'users'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'date_from' => 'required|date',
            'date_to' => 'required|date|after_or_equal:date_from',
            'user_id' => 'required'
        ]);
        $requestData = $request->all();

        $vacation = Vacation::findOrFail($id);
        $vacation->update($requestData);
        
        return redirect('admin/vacation')->with('flash_message', 'Vacation updated!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Vacation::destroy($id);
        
        return redirect('admin/vacation')->with('flash_message', 'Vacation deleted!');
    }
}
