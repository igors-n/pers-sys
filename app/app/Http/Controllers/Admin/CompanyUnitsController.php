<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CompanyUnit;
use Illuminate\Http\Request;

class CompanyUnitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $companyUnits = CompanyUnit::where('name', 'LIKE', "%$keyword%")
                ->orWhere('company', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $companyUnits = CompanyUnit::latest()->paginate($perPage);
        }

        return view('admin.company-units.index', compact('companyUnits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $companiesObject = Company::select('id', 'name')->get();
        $companies = [null => '---'];
        foreach ($companiesObject as $company) {
            $companies[$company->id] = $company->name;
        }

        return view('admin.company-units.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'company' => 'required'
		]);
        $requestData = $request->all();
        
        CompanyUnit::create($requestData);

        return redirect('admin/company-units')->with('flash_message', 'CompanyUnit added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $companyUnit = CompanyUnit::findOrFail($id);

        return view('admin.company-units.show', compact('companyUnit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $companyUnit = CompanyUnit::findOrFail($id);
        $companiesObject = Company::select('id', 'name')->get();
        $companies = [null => '---'];
        foreach ($companiesObject as $company) {
            $companies[$company->id] = $company->name;
        }

        return view('admin.company-units.edit', compact('companyUnit', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'company' => 'required'
		]);
        $requestData = $request->all();
        
        $companyUnit = CompanyUnit::findOrFail($id);
        $companyUnit->update($requestData);

        return redirect('admin/company-units')->with('flash_message', 'CompanyUnit updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CompanyUnit::destroy($id);

        return redirect('admin/company-units')->with('flash_message', 'CompanyUnit deleted!');
    }
}
