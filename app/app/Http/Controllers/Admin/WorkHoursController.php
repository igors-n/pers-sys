<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\WorkHour;
use Illuminate\Http\Request;

class WorkHoursController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $workhours = WorkHour::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('hours', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        }
        else if ($request->get('date_from') || $request->get('date_to')) {
            if ($request->get('date_from') && $request->get('date_to')) {
                $workhours = WorkHour::where('date', '>=', $request->get('date_from'))
                    ->where('date', '<=', $request->get('date_to'))->latest()->paginate($perPage);
            }
            else if ($request->get('date_from')) {
                $workhours = WorkHour::where('date', '>=', $request->get('date_from'))
                    ->latest()->paginate($perPage);
            }
            else {
                $workhours = WorkHour::where('date', '<=', $request->get('date_from'))
                    ->latest()->paginate($perPage);
            }
        }
        else {
            $workhours = WorkHour::latest()->paginate($perPage);
        }

        return view('admin.work-hours.index', compact('workhours'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $usersObject = User::select('id', 'name', 'last_name')->get();
        $users = [NULL => '---'];
        foreach ($usersObject as $user) {
            $users[$user->id] = $user->name . ' ' . $user->last_name;
        }
        return view('admin.work-hours.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'user_id' => 'required',
            'date' => 'required|date',
            'hours' => 'required|integer|min:0',
        ]);
        $requestData = $request->all();

        WorkHour::create($requestData);

        return redirect('admin/work-hours')->with('flash_message', 'WorkHour added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $workhour = WorkHour::findOrFail($id);

        return view('admin.work-hours.show', compact('workhour'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $workhour = WorkHour::findOrFail($id);
        $usersObject = User::select('id', 'name', 'last_name')->get();
        $users = [NULL => '---'];
        foreach ($usersObject as $user) {
            $users[$user->id] = $user->name . ' ' . $user->last_name;
        }

        return view('admin.work-hours.edit', compact('workhour', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'user_id' => 'required',
            'date' => 'required|date',
            'hours' => 'required|integer|min:0',
        ]);
        $requestData = $request->all();

        $workhour = WorkHour::findOrFail($id);
        $workhour->update($requestData);

        return redirect('admin/work-hours')->with('flash_message', 'WorkHour updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        WorkHour::destroy($id);

        return redirect('admin/work-hours')->with('flash_message', 'WorkHour deleted!');
    }

    /**
     * Table with grouped working hours
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function table(Request $request) {
        if ($request->get('date_from') || $request->get('date_to')) {
            if ($request->get('date_from') && $request->get('date_to')) {
                $workhours = WorkHour::groupBy('user_id')
                    ->selectRaw('sum(hours) as sum, user_id')->where('date', '>=', $request->get('date_from'))
                    ->where('date', '<=', $request->get('date_to'))->get();
            }
            else if ($request->get('date_from')) {
                $workhours = WorkHour::groupBy('user_id')
                    ->selectRaw('sum(hours) as sum, user_id')
                    ->where('date', '>=', $request->get('date_from'))
                    ->get();
            }
            else {
                $workhours = WorkHour::groupBy('user_id')
                    ->selectRaw('sum(hours) as sum, user_id')
                    ->where('date', '<=', $request->get('date_from'))
                    ->get();
            }
        }
        else {
            $workhours = WorkHour::groupBy('user_id')
                ->selectRaw('sum(hours) as sum, user_id')
                ->get();
        }

        return view('admin.work-hours.table', compact('workhours'));
    }
}
