<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Experience;
use Illuminate\Http\Request;

class ExperienceController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $keyword = $request->get('search');
        $perPage = 25;
        
        if (!empty($keyword)) {
            $experience = Experience::where('user_id', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('date_from', 'LIKE', "%$keyword%")
                ->orWhere('date_to', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $experience = Experience::latest()->paginate($perPage);
        }
        
        return view('admin.experience.index', compact('experience'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $usersObject = User::select('id', 'name', 'last_name')->get();
        $users = [null => '---'];
        foreach ($usersObject as $user) {
            $users[$user->id] = $user->name . ' ' . $user->last_name;
        }
        return view('admin.experience.create', compact('users'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required',
            'date_from' => 'required|date',
            'date_to' => 'required|date|after_or_equal:date_from'
        ]);
        $requestData = $request->all();
        
        Experience::create($requestData);
        
        return redirect('admin/experience')->with('flash_message', 'Experience added!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        $experience = Experience::findOrFail($id);
        
        return view('admin.experience.show', compact('experience'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $experience = Experience::findOrFail($id);
        $usersObject = User::select('id', 'name', 'last_name')->get();
        $users = [null => '---'];
        foreach ($usersObject as $user) {
            $users[$user->id] = $user->name . ' ' . $user->last_name;
        }
        
        return view('admin.experience.edit', compact('experience', 'users'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'user_id' => 'required',
            'name' => 'required',
            'date_from' => 'required|date',
            'date_to' => 'required|date|after_or_equal:date_from'
        ]);
        $requestData = $request->all();
        
        $experience = Experience::findOrFail($id);
        $experience->update($requestData);
        
        return redirect('admin/experience')->with('flash_message', 'Experience updated!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Experience::destroy($id);
        
        return redirect('admin/experience')->with('flash_message', 'Experience deleted!');
    }
}
