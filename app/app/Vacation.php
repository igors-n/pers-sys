<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Vacation extends Model
{
    use LogsActivity;
    
    public const TYPES = [
        0 => 'Vacation',
        1 => 'Sick leave',
        2 => 'Business trip',
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vacations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_from', 'date_to', 'user_id', 'type'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    

    /**
     * Change activity log event description
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getDescriptionForEvent($eventName)
    {
        return __CLASS__ . " model has been {$eventName}";
    }
}
